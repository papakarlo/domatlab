function sig_fft_2
% freq + noise
Fs = 100000000;
F = 10000000;
for i = 1:1:20
    y = sin_int16(1024,Fs,F); % function
    subplot(2,1,1); 
    plot(y,'b')
    % plot spectr
    Y = double(y)/32768;
    %w = hamming(length(Y));%������� ����
    w = blackman(length(Y));%������� ����
    Y=(w'.*Y);%������������ ��������
    Y = fft(Y);
    Pyy = Y.*conj(Y)/(1024*1024);
    %���������� �������� �������
    f = Fs*(1:512)/1024;
    %f = 1:512;
    subplot(2,1,2); 
    b = max(Pyy);
    plot(f,10*log10(Pyy(1:512)/b),'b')
    grid on;
    pause(0.1);
end
close all

function y = sin_int16(size,Fs,F)
    div = Fs/F;
    t = 0:1/div:(size-1)/div;
    r = randi([-16,16],[1,size],'int16');
    ph = randn;
    x = int16(32768*sin(2*pi*t + ph));
    y = x+r;
end

end



