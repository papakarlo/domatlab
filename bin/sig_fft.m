% freq + noise
Fs = 100000000;
F = 10000000;
div = Fs/F;
t = 0:1/div:(1023/div);
for i = 1:1:20
    ph = randn;
    x = int16(32768*sin(2*pi*t + ph));
    r = randi([-16,16],[1,1024],'int16');
    %r = randi_mex([-16,16],[1,1024],'int16');
    y = x+r;
    subplot(2,1,1); 
    plot(y,'b')
    % plot spectr
    Y = double(y)/32768;
    %w = hamming(length(Y));%������� ����
    w = blackman(length(Y));%������� ����
    Y=(w'.*Y);%������������ ��������
    Y = fft(Y);
    Pyy = Y.*conj(Y)/(1024*1024);
    %���������� �������� �������
    %f = Fs*(1:512)/(1024);
    f = 1:512;
    subplot(2,1,2); 
    b = max(Pyy);
    plot(f,10*log10(Pyy(1:512)/b),'b')
    grid on;
    pause(0.1);
end
close all



