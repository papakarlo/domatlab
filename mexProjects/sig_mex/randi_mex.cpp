
#include <stdlib.h>
#include <string.h>

#include "mex.h"


void randi_mex_16( short *data, double range_min, double range_max, int n )
{
     // Generate random numbers in the half-closed interval
     // [range_min, range_max]. In other words,
     // range_min <= random number < range_max
     int i;
     for ( i = 0; i < n; i++ )
     {
        int u = (double)rand() / (RAND_MAX + 1) * (range_max - range_min) + range_min;
		data[i] = u;
     }
}

void randi_mex_8( char *data, double range_min, double range_max, int n )
{
     // Generate random numbers in the half-closed interval
     // [range_min, range_max]. In other words,
     // range_min <= random number < range_max
     int i;
     for ( i = 0; i < n; i++ )
     {
        char u = (double)rand() / (RAND_MAX + 1) * (range_max - range_min) + range_min;
		data[i] = u;
     }
}

//---------------------------------------------------------------------------
// prhs[0] - range array [from, to]
// prhs[1] - index array [from, to]
// prhs[2] - type ('int8', 'int16', 'int32')
// plhs[0] - output array 
//
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//To check for 3 input arguments, use this code.
	if(nrhs != 3) {
		mexErrMsgIdAndTxt("MyToolbox:randi_mex:nrhs",
                      "3 inputs required.");
	}

	// Use this code to check for one output argument, the product outMatrix.
	if(nlhs != 1) {
		mexErrMsgIdAndTxt("MyToolbox:randi_mex:nlhs",
                      "1 output required.");
	}

	//This code validates that in1, represented by prhs[0], is type double.
	if( !mxIsDouble(prhs[0]) ) {
			mexErrMsgIdAndTxt("MyToolbox:add_mex:notDouble",
				"Input matrix 1 must be type double.");
	}     
	
	//This code validates that in2, represented by prhs[1], is type double.
	if( !mxIsDouble(prhs[1]) ) {
			mexErrMsgIdAndTxt("MyToolbox:add_mex:notDouble",
				"Input matrix 2 must be type double.");
	}	
	
	//This code validates that in3, represented by prhs[2], is type char.
	if( !mxIsChar(prhs[2]) ) {
			mexErrMsgIdAndTxt("MyToolbox:add_mex:notChar",
				"Input 3 must be type char.");
	}	

	// check that number of rows in first input argument is 2
	int x = mxGetM(prhs[0]);
	if(mxGetN(prhs[0]) != 2) {
		mexErrMsgIdAndTxt("MyToolbox:add_mex:notRowVector",
			              "Input 1 must be a row vector [,].");
	}	

	// check that number of rows in second input argument is 2 
	if(mxGetN(prhs[1]) != 2) {
		mexErrMsgIdAndTxt("MyToolbox:add_mex:notRowVector",
			              "Input 2 must be a row vector [,].");
	}
	
	double  *in1;			// 1x2 input matrix 
	double  *in2;			// 1x2 input matrix 
	//mxChar *in3;			// string
	mwSize ncols;		// size of out matrix 

	// create a pointer to the real data in the input matrix  
	in1 = mxGetPr(prhs[0]);
	in2 = mxGetPr(prhs[1]);
    //in3 = mxGetChars(prhs[2]); 

	// get dimensions of the output matrix 
	ncols = in2[1]-in2[0]+1;

	char *in3 = mxArrayToString(prhs[2]);

	if( strcmp(in3,(char*)"int16") == 0) {
		plhs[0] = mxCreateNumericMatrix(1, ncols, mxINT16_CLASS, mxREAL);
		short *outMatrix = (short int*)mxGetPr(plhs[0]);		// output matrix 
		randi_mex_16( outMatrix, in1[0], in1[1], ncols );
	} else if( strcmp(in3,(char*)"int8") == 0) {
		plhs[0] = mxCreateNumericMatrix(1, ncols, mxINT8_CLASS, mxREAL);
		char *outMatrix = (char*)mxGetPr(plhs[0]);		// output matrix 
		randi_mex_8( outMatrix, in1[0], in1[1], ncols );
	}

}

