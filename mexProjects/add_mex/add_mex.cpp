
#include "mex.h"

#include <stdlib.h>

void add_array(double *x, double *y, double *z, int n)
{
  int i;
  for (i=0; i<n; i++) {
    z[i] = x[i] + y[i];
  }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//To check for two input arguments, use this code.
	if(nrhs != 2) {
		mexErrMsgIdAndTxt("MyToolbox:add_mex:nrhs",
                      "Two inputs required.");
	}

	// Use this code to check for one output argument, the product outMatrix.
	if(nlhs != 1) {
		mexErrMsgIdAndTxt("MyToolbox:add_mex:nlhs",
                      "One output required.");
	}

	//This code validates that inMatrix, represented by prhs[0], is type double.
	if( !mxIsDouble(prhs[0]) || 
		mxIsComplex(prhs[0])) {
			mexErrMsgIdAndTxt("MyToolbox:add_mex:notDouble",
				"Input matrix must be type double.");
	}     
	
	//This code validates that inMatrix, represented by prhs[1], is type double.
	if( !mxIsDouble(prhs[1]) || 
		mxIsComplex(prhs[1])) {
			mexErrMsgIdAndTxt("MyToolbox:add_mex:notDouble",
				"Input matrix must be type double.");
	}	
	
	// check that number of rows in first input argument is 1 
	if(mxGetM(prhs[0]) != 1) {
		mexErrMsgIdAndTxt("MyToolbox:add_mex:notRowVector",
			              "Input must be a row vector.");
	}	

	// check that number of rows in second input argument is 1 
	if(mxGetM(prhs[1]) != 1) {
		mexErrMsgIdAndTxt("MyToolbox:add_mex:notRowVector",
			              "Input must be a row vector.");
	}
	
	double *inMatrix_X;			// 1xN input matrix 
	double *inMatrix_Y;			// 1xN input matrix 
	mwSize ncols;				// size of matrix 
	double *outMatrix;			// output matrix 

	// create a pointer to the real data in the input matrix  
	inMatrix_X = mxGetPr(prhs[0]);
	inMatrix_Y = mxGetPr(prhs[1]);

	// get dimensions of the input matrix 
	ncols = __min(mxGetN(prhs[0]),mxGetN(prhs[1]));

	// create the output matrix 
	plhs[0] = mxCreateDoubleMatrix(1,ncols,mxREAL);
	// get a pointer to the real data in the output matrix 
	outMatrix = mxGetPr(plhs[0]);

	add_array(inMatrix_X, inMatrix_Y, outMatrix, ncols);

}

